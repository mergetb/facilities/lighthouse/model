# LighthouseTB

LighthouseTB is a Merge testbed facility featuring 96 servers and advanced
virtualization support.

## Experiment Network

The experiment network is depicted below.

![](diagrams/xpnet.png)

The nodes are patitioned into two categories

- **Bouy:** is the standard node for hosting virtual machines. Connected at 40
  gbps.
- **Beacon:** is a high bandwidth node connected at 200 gbps for network
  emulation network-heavy workloads.

Both the Bouy and Beacon nodes have 2x Intel Xeon E5 2650v5 CPUs (12 cores a
piece) with 192 GB of memory, 3.9 TB of SSD space and 1TB of HDD.

## Infrastructure Network

The infrastructure network connects nodes to each other and to mass storage. The
Bouys are connected at 10G to the infranet and the Beacons at 1G. The extra
infranet capacity on the Bouys is to support better remote storage performance.

![](diagrams/infranet.png)

