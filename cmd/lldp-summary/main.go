package main

import (
	"gitlab.com/mergetb/facilities/lighthouse/model"
	"log"
	"os"
)

func main() {

	if len(os.Args) <= 1 {
		log.Fatal("usage: lldp-summary <file.json>")
	}

	lighthouse.SummarizeLLDP(os.Args[1])

}
