package lighthouse

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"text/tabwriter"

	log "github.com/sirupsen/logrus"

	. "gitlab.com/mergetb/xir/v0.3/go/build"
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)

type LLDP struct {
	Ifx []LLDPInfo `json:"interface"`
}
type LLDPInfos struct {
	Lldp []LLDP
}
type LLDPInfo struct {
	Name    string
	Chassis []LLDPChassis
	Port    []LLDPPort
}
type LLDPChassis struct {
	Id   []LLDPTV
	Name []LLDPTV
}
type LLDPPort struct {
	Id    []LLDPTV
	Descr []LLDPTV
}
type LLDPTV struct {
	Type  string
	Value string
}

func SummarizeLLDP(filename string) error {

	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		return fmt.Errorf("read %s: %v", filename, err)
	}

	var info LLDPInfos
	err = json.Unmarshal(buf, &info)
	if err != nil {
		return fmt.Errorf("unmarshal %s: %v", filename, err)
	}

	// hacking in model relevant names in place of rack position names
	m := map[string]string{
		"mgmtr1s45": "mg0",
		"xpfr3s42":  "xpf",
		"xplr1s21":  "xpl0",
		"xplr1s42":  "xpl1",
		"xplr2s21":  "xpl2",
		"xplr2s42":  "xpl3",
		"xpsr1s43":  "xps0",
		"xpsr2s43":  "xps1",
	}

	for _, x := range info.Lldp {
		for _, entry := range x.Ifx {
			if len(entry.Chassis) == 0 {
				continue
			}
			if len(entry.Port) == 0 {
				continue
			}
			if len(entry.Chassis[0].Name) == 0 {
				continue
			}
			if len(entry.Port[0].Descr) == 0 {
				continue
			}
			if len(entry.Port[0].Id) == 0 {
				continue
			}

			remote := entry.Chassis[0].Name[0].Value
			n, ok := m[remote]
			if ok {
				remote = n
			}

			fmt.Fprintf(tw, "[%s]\t->\t%s\t[%s]\t%s\n",
				entry.Name,
				remote, //entry.Chassis[0].Name[0].Value,
				entry.Port[0].Descr[0].Value,
				entry.Port[0].Id[0].Value,
			)
		}
	}

	tw.Flush()

	return nil

}

// NOTE: These functions assume that files are named lldp-<switch>.json where
// <switch> is the name of a switch as defined in the model.

func gatherLLDPInfo() (map[string]*LLDPInfos, error) {

	lldpdir := os.Getenv("LLDPDIR")
	if lldpdir == "" {
		lldpdir = "."
	}

	files, err := ioutil.ReadDir(lldpdir)
	if err != nil {
		return nil, fmt.Errorf("read dir %s: %v", lldpdir, err)
	}

	m := make(map[string]*LLDPInfos)

	for _, f := range files {

		if filepath.Ext(f.Name()) == ".json" {

			filename := fmt.Sprintf("%s/%s", lldpdir, f.Name())

			buf, err := ioutil.ReadFile(filename)
			if err != nil {
				return nil, fmt.Errorf("read file %s: %v", filename, err)
			}

			info := new(LLDPInfos)
			err = json.Unmarshal(buf, info)
			if err != nil {
				continue
			}
			if len(info.Lldp) == 0 {
				continue
			}
			switchName := strings.TrimPrefix("lldp-", f.Name())
			m[switchName] = info

		}

	}

	return m, nil

}

// TODO: Incomplete, see comment in topo.go:165

func applyLLDPInfo(tb *Builder) error {

	m, err := gatherLLDPInfo()
	if err != nil {
		return err
	}

	for switchName, info := range m {

		sw := tb.GetResource(switchName)
		if sw == nil {
			log.Warnf(
				"switch %s not found in model, skipping applying LLDP info",
				switchName,
			)
			continue
		}

		for _, lldp := range info.Lldp {
			for _, entry := range lldp.Ifx {

				if len(entry.Chassis) == 0 {
					continue
				}
				if len(entry.Port) == 0 {
					continue
				}

			}
		}
	}

	return nil

}
