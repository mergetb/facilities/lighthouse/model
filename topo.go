package lighthouse

import (
	"fmt"
	"log"

	xir "gitlab.com/mergetb/xir/v0.3/go"
	. "gitlab.com/mergetb/xir/v0.3/go/build"
)

var (
	buoys   []*xir.Resource
	beacons []*xir.Resource
	emus    []*xir.Resource

	mleaf    []*xir.Resource
	mspine   *xir.Resource
	ispine   *xir.Resource
	ileaf1g  *xir.Resource
	ileaf40g []*xir.Resource
	xleaf    []*xir.Resource
	xleaf64  *xir.Resource
	xspine   []*xir.Resource

	infraservers []*xir.Resource
	stor         []*xir.Resource
	ops          *xir.Resource
)

func name(prefix string, id int) string {
	if id < 10 {
		return fmt.Sprintf("%s00%d", prefix, id)
	} else {
		return fmt.Sprintf("%s0%d", prefix, id)
	}
}

func buildBuoys(tb *Builder) []*xir.Resource {
	b := tb.Nodes(80, "buoy",
		Procs(2, Cores(12), Reserved(1), Threads(24)),
		Dimms(1, GB(8), Reserved(xir.GB(4))),
		Dimms(7, GB(8)),
		Dimms(8, GB(16)),
		SSDs(1, GB(120)),
		SSDs(2, GB(1920)),
		HDDs(1, GB(1024)),
		Ipmi(1, Mbps(100)),       /// not cabled
		Eno(2, Gbps(1), Mgmt(0)), // first is combo BMC/IPMI + eth0
		Eth(1, Gbps(10), Infranet()),
		Eth(4, Gbps(10), Xpnet()),
		AllocModes(
			xir.AllocMode_Physical,
			xir.AllocMode_Virtual,
		),
		Roles(
			xir.Role_TbNode,
			xir.Role_Hypervisor,
		),
		BMCIpmi(),
		Product("Supermicro", "Custom Buoy Server", "BUOY/CSE-514-R400W/MBD-X10DRW-ET"),
		Rootdev("/dev/sda"),
		DefaultImage("bullseye"),
		Uefi(),
	)

	// fix port names
	for _, buoy := range b {
		// infranet
		buoy.NICs[2].Ports[0].Name = "ens2np0"

		// xpnet
		buoy.NICs[3].Ports[0].Name = "ens1f0"
		buoy.NICs[3].Ports[1].Name = "ens1f1"
		buoy.NICs[3].Ports[2].Name = "ens1f2"
		buoy.NICs[3].Ports[3].Name = "ens1f3"
	}

	// the last five are entirely uncabled/unracked; don't even know MACs; mark them as NoAlloc
	// these nodes have no infranet MAC addresses; we need to disable them before attempting to
	// commission the model
	noInfra := []uint{60, 68, 75, 76, 77, 78, 79}
	for _, idx := range noInfra {
		buoy := b[idx]
		buoy.Alloc = append(buoy.Alloc, xir.AllocMode_NoAlloc)
	}

	// buoy025 has a bad port; "disable" it by just removing the XpLink role
	b[25].NICs[3].Ports[1].Role = xir.LinkRole_LinkRole_Unspecified

	return b
}

func buildBeacons(tb *Builder) []*xir.Resource {
	b := tb.Nodes(10, "beacon",
		Procs(2, Cores(12), Reserved(1), Threads(24)),
		Dimms(1, GB(8), Reserved(xir.GB(4))),
		Dimms(7, GB(8)),
		Dimms(8, GB(16)),
		SSDs(1, GB(120)),
		SSDs(2, GB(1920)),
		HDDs(1, GB(1024)),
		Ipmi(1, Mbps(100)),                    // not cabled
		Eno(2, Gbps(1), Mgmt(0), Infranet(1)), // combo BMC/IPMI + eth0
		Eth(2, Gbps(100), Xpnet()),
		AllocModes(
			xir.AllocMode_Physical,
			xir.AllocMode_Virtual,
		),
		Roles(
			xir.Role_TbNode,
			xir.Role_Hypervisor,
		),
		BMCIpmi(),
		Product("Supermicro", "Custom Beacon Server", "BEACON/CSE-514-R400W/MBD-X10DRW-ET"),
		Rootdev("/dev/sda"),
		DefaultImage("bullseye"),
		Uefi(),
	)

	// fix port names
	for _, beacon := range b {
		// xpnet
		beacon.NICs[2].Ports[0].Name = "ens1np0"
		beacon.NICs[2].Ports[1].Name = "ens2np0"
	}

	return b
}

func buildEmus(tb *Builder) []*xir.Resource {
	emus := tb.NetworkEmulators(6, "emu",
		Ipmi(1, Mbps(100)),                    // not cabled
		Eno(2, Gbps(1), Mgmt(0), Infranet(1)), // combo BMC/IPMI + eth0
		Eth(2, Gbps(100), Xpnet()),
		BMCIpmi(),
		Product("Supermicro", "Custom Beacon Server", "BEACON/CSE-514-R400W/MBD-X10DRW-ET"),
		SSDs(1, GB(1920), SysDisk(), Sata3()),
		SSDs(1, GB(1920)),
		SSDs(1, GB(120)),
		HDDs(1, GB(1024)),
		Rootdev("/dev/sda"),
		Uefi(),
	)

	for _, emu := range emus {
		// xpnet
		emu.NICs[2].Ports[0].Name = "ens1np0"
		emu.NICs[2].Ports[1].Name = "ens2np0"
		emu.NICs[2].Ports[0].Bond = &xir.PortBond{
			Name: "xleaf64",
		}
		emu.NICs[2].Ports[1].Bond = &xir.PortBond{
			Name: "xleaf64",
		}
	}

	return emus
}

func Topo() *xir.Facility {

	tb, err := NewBuilder(
		"lighthouse",
		"ops.lighthousetb.net",
	)
	if err != nil {
		log.Fatalf("new builder: %v", err)
	}

	IndexPad = 3

	buoys = buildBuoys(tb)
	beacons = buildBeacons(tb)
	emus = buildEmus(tb)

	IndexPad = 0

	// Ops server
	ops = tb.OpsServer("ops",
		Procs(1, Cores(16), Threads(32)),
		Dimms(8, GB(16)),
		NVMEs(1, 0, GB(4000)),
		NVMEs(1, 1, GB(4000)),
		Ipmi(1, Mbps(100)),
		Eno(1, Gbps(1), Mgmt()),
		Eno(2, Gbps(1)),
		Enp(4, 1),
	)
	ops.Mgmt().Mac = "18:c0:4d:06:bb:e3"
	ops.NICs[3].Ports[0].Name = "enp193s0f0"
	ops.NICs[3].Ports[1].Name = "enp193s0f1"
	ops.NICs[3].Ports[2].Name = "enp193s0f2"
	ops.NICs[3].Ports[3].Name = "enp193s0f3"

	// Infrastruct server
	//infraservers = tb.Infraservers(2, "infra",
	infraservers = tb.Infraservers(2, "ifr",
		Procs(2, Cores(32)),
		Dimms(32, GB(16)),
		NVMEs(1, 0, GB(4000), EtcdDisk()),
		NVMEs(1, 1, GB(4000), MinioDisk()),
		NVMEs(1, 2, GB(1000), SysDisk()),
		Ipmi(1, Mbps(100)),       // not cabled
		Eno(2, Gbps(1), Mgmt(0)), // combo BMC/IPMI
		Eno(2, Gbps(100), Gw(0), Infranet(1)),
		Product("Gigabyte", "Custom Infrapod Server", "INFRASERVER/CX1265g-NVMe-E8"),
		Rootdev("/dev/nvme2n1"),
		BMCIpmi(),
		RoleAdd(xir.Role_BorderGateway),
		RoleAdd(xir.Role_Gateway),
		RoleAdd(xir.Role_EtcdHost),
		RoleAdd(xir.Role_MinIOHost),
		RoleAdd(xir.Role_SledHost),
	)

	// manually set infrapod roles (a limited subset of infraserver roles)
	infraservers[1].Roles = []xir.Role{
		xir.Role_InfrapodServer,
		xir.Role_Gateway,
		xir.Role_BorderGateway, // XXX: should not be needed
	}

	// set port names correctly
	for _, ifr := range infraservers {
		ifr.NICs[2].Ports[0].Name = "enp33s0f0np0"
		ifr.NICs[2].Ports[1].Name = "enp33s0f1np1"
	}

	//TODO add storage back in
	/*
		stor = tb.StorageServers(2, "stor",
			Procs(2, Cores(32)),
			Dimms(32, GB(16)),
			SSDs(24, TB(2)),
			Ipmi(1, Mbps(100)),
			Eth(2, Gbps(1), Mgmt()),
			Eth(2, Gbps(100), Infranet()),
			Product("Gigabyte", "Custom Storage Server", "STOR/CX2265g-NVMe-E8"),
		)
	*/

	ileaf1g = tb.InfraLeaf("ileaf1g",
		Eth(1, Gbps(1), Mgmt()),
		Swp(48, Gbps(1), Infranet()),
		Swp(4, Gbps(10), Infranet()),
		Product("EdgeCore", "52 port 1G/10G switch", "AS4610"),
	)

	ileaf40g = tb.InfraLeaves(2, "ileaf40g",
		Eth(1, Gbps(1), Mgmt()),
		Swp(32, Gbps(40), Infranet()),
		Product("EdgeCore", "32 port 40G switch", "AS6712"),
		RoleAdd(xir.Role_Stem),
	)

	ispine = tb.InfraSpine("ispine",
		Eth(1, Gbps(1), Mgmt()),
		Swp(32, Gbps(40), Infranet()),
		Product("EdgeCore", "32 port 40G switch", "AS6712"),
		RoleAdd(xir.Role_Stem),
	)

	xleaf = tb.XpLeaves(4, "xleaf",
		Eth(1, Gbps(1), Mgmt()),
		Swp(32, Gbps(100), Xpnet()),
		Product("Mellanox", "Spectrum-2 32 port 100G switch", "MSN3700-CS2FC"),
		RoleAdd(xir.Role_Stem),
	)

	xleaf64 = tb.XpLeaf("xleaf64",
		Eth(1, Gbps(1), Mgmt()),
		Swp(64, Gbps(100), Xpnet()),
		Product("Mellanox", "Spectrum-3 64 port 100G switch", "MSN4600-C2FC"),
		RoleAdd(xir.Role_Stem),
	)

	xspine = tb.XpSpines(2, "xspine",
		Eth(1, Gbps(1), Mgmt()),
		Swp(32, Gbps(100), Xpnet()),
		Product("Mellanox", "Spectrum-2 32 port 100G switch", "MSN3700-CS2FC"),
	)

	mleaf = tb.MgmtLeaves(4, "mleaf",
		Eth(1, Gbps(1)),
		Swp(48, Gbps(1), Mgmt()),
		Swp(4, Gbps(10), Mgmt()),
		Product("Quanta", "52 port 1G/10G switch", "BMST1024-LY4R"),
	)

	mspine = tb.MgmtSpine("mspine",
		Eth(1, Gbps(1)),
		Swp(48, Gbps(10), Mgmt()),
		Swp(6, Gbps(40), Mgmt()),
		Product("Quanta", "54 port 10G/50G switch", "BMST3048-LY8"),
	)

	//idealCabling(tb)
	actualCabling(tb)

	tbf, err := tb.Facility()
	if err != nil {
		log.Fatal(err)
	}

	return tbf

}

func actualCabling(tb *Builder) {

	// TODO there is a start for something along these lines in lldp.go but it
	// needs more thought in terms of how things like breakout cables will be
	// managed, how references to ports as they are reported by LLDP map onto
	// ports in the model (e.g. this can vary depending on what OS is loaded,
	// but the physical reality remains constant)
	//
	// applyLLDPInfo(tb)
	//
	// Thus I've elected just to code up the model based on recognizing the
	// patterns of how the system is cabled from the LLDP data and using my
	// brain to transform that information into an accurate representative model
	//
	// Automagically transforming LLDP specs into a cabling model is a laudable
	// goal and should be pursued, but not in the context of racing to get the
	// Lighthouse system up and running.

	numEmus := len(emus)
	numBeacons := len(beacons)

	// Leaf switch to node connections ========================================

	// Mgmt leaf 0 ............................................................

	for i := 0; i < 40; i++ {
		tb.Connect(mleaf[0].NICs[1].Ports[i], buoys[i].Mgmt())
	}

	// Mgmt leaf 1 ............................................................

	for i := 0; i < 40; i++ {
		tb.Connect(mleaf[1].NICs[1].Ports[i], buoys[40+i].Mgmt())
	}

	// Mgmt leaf 2 ............................................................

	for i := 0; i < numBeacons; i++ {
		tb.Connect(mleaf[2].NICs[1].Ports[i], beacons[i].Mgmt())
	}

	for i := 0; i < numEmus; i++ {
		j := numBeacons + i
		eidx := numEmus - 1 - i

		// emu000/emu001 inverted
		if eidx == 0 {
			eidx = 1
		} else if eidx == 1 {
			eidx = 0
		}

		tb.Connect(mleaf[2].NICs[1].Ports[j], emus[eidx].Mgmt())
	}

	// Infra leaf 0 ...........................................................

	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(2), iPorts(buoys[0:4]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(4), iPorts(buoys[4:8]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(6), iPorts(buoys[8:12]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(8), iPorts(buoys[12:16]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(10), iPorts(buoys[16:20]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(24), iPorts(buoys[20:24]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(26), iPorts(buoys[24:28]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(28), iPorts(buoys[28:32]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(30), iPorts(buoys[32:36]))
	tb.Breakout(ileaf40g[0].NICs[1], ileaf40g[0].NICs[1].PhysPort(32), iPorts(buoys[36:40]))

	// Infra leaf 1 ...........................................................

	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(2), iPorts(buoys[40:44]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(4), iPorts(buoys[44:48]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(6), iPorts(buoys[48:52]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(8), iPorts(buoys[52:56]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(10), iPorts(buoys[56:60]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(24), iPorts(buoys[60:64]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(26), iPorts(buoys[64:68]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(28), iPorts(buoys[68:72]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(30), iPorts(buoys[72:76]))
	tb.Breakout(ileaf40g[1].NICs[1], ileaf40g[1].NICs[1].PhysPort(32), iPorts(buoys[76:80]))

	// Infra leaf 2 ...........................................................
	for i := 0; i < numBeacons; i++ {
		tb.Connect(ileaf1g.NICs[1].Ports[i], beacons[i].Infranet())
	}

	for i := 0; i < numEmus; i++ {
		j := numBeacons + i
		eidx := numEmus - 1 - i

		// emu000/emu001 inverted
		if eidx == 0 {
			eidx = 1
		} else if eidx == 1 {
			eidx = 0
		}

		tb.Connect(ileaf1g.NICs[1].Ports[j], emus[eidx].Infranet())
	}

	// XP leaf 0-3 ............................................................
	buoy := 0

	for xpl := 0; xpl <= 3; xpl++ {
		for i := 0; i < 10; i++ {

			// The buoy xpnet ports are connected to the xleaf[0,1,2,3] switches through breakout cables. However, the
			// connection order is inverted:
			// buoy port0 -> breakout port 3
			// buoy port1 -> breakout port 2
			// buoy port2 -> breakout port 1
			// buoy port3 -> breakout port 0
			tb.Breakout(
				xleaf[xpl].NICs[1],
				xleaf[xpl].NICs[1].PhysPort(i+1),
				[]*xir.Port{
					buoys[buoy].NICs[3].Ports[3],
					buoys[buoy].NICs[3].Ports[2],
					buoys[buoy].NICs[3].Ports[1],
					buoys[buoy].NICs[3].Ports[0],
				},
			)
			buoy++
		}
		for i := 22; i < 32; i++ {
			tb.Breakout(
				xleaf[xpl].NICs[1],
				xleaf[xpl].NICs[1].PhysPort(i+1),
				[]*xir.Port{
					buoys[buoy].NICs[3].Ports[3],
					buoys[buoy].NICs[3].Ports[2],
					buoys[buoy].NICs[3].Ports[1],
					buoys[buoy].NICs[3].Ports[0],
				},
			)
			buoy++
		}
	}

	// XP fabric ..............................................................

	for i, b := range beacons {
		p := 2 + (i * 4)
		// NOTE: the inverted order of the beacon ports is intentional
		tb.Connect(xleaf64.NICs[1].Ports[p], b.NICs[2].Ports[1])
		tb.Connect(xleaf64.NICs[1].Ports[p+1], b.NICs[2].Ports[0])
	}

	// emu ports are bonded
	for i := 0; i < numEmus; i++ {
		j := i + numBeacons // next xleaf64 port
		p := 2 + (j * 4)    //
		eidx := numEmus - 1 - i

		// emu000/emu001 inverted
		if eidx == 0 {
			eidx = 1
		} else if eidx == 1 {
			eidx = 0
		}

		e := emus[eidx]

		// NOTE: the inverted order of the emu ports is intentional
		tb.Trunk(
			xleaf64.NICs[1].Ports[p:p+2],
			[]*xir.Port{
				e.NICs[2].Ports[1],
				e.NICs[2].Ports[0],
			},
			e.Id,
			xleaf64.Id,
		)
	}

	// Inter-switch connections ===============================================

	// mleaf-mspine ...........................................................

	tb.Connect(mleaf[0].NICs[2].Ports[0], mspine.NICs[1].Ports[9])

	tb.Connect(mleaf[1].NICs[2].Ports[0], mspine.NICs[1].Ports[6])

	tb.Connect(mleaf[2].NICs[2].Ports[0], mspine.NICs[1].Ports[4])
	tb.Connect(mleaf[2].NICs[2].Ports[1], mspine.NICs[1].Ports[5])

	tb.Connect(mleaf[3].NICs[2].Ports[0], mspine.NICs[1].Ports[2])
	tb.Connect(mleaf[3].NICs[2].Ports[1], mspine.NICs[1].Ports[3])

	// ileaf-ispine ...........................................................

	/* XXX
	tb.Connect(ispine.NICs[1].Ports[22], ileaf40g[0].NICs[1].PhysPort(13))
	tb.Connect(ispine.NICs[1].Ports[23], ileaf40g[0].NICs[1].PhysPort(14))
	tb.Connect(ispine.NICs[1].Ports[24], ileaf40g[0].NICs[1].PhysPort(15))
	tb.Connect(ispine.NICs[1].Ports[25], ileaf40g[0].NICs[1].PhysPort(16))
	*/
	tb.Trunk(
		ispine.NICs[1].Ports[22:26],
		ileaf40g[0].NICs[1].PhysPorts(13, 17),
		ileaf40g[0].Id,
		ispine.Id,
	)

	/* XXX
	tb.Connect(ispine.NICs[1].Ports[26], ileaf40g[1].NICs[1].PhysPort(13))
	tb.Connect(ispine.NICs[1].Ports[27], ileaf40g[1].NICs[1].PhysPort(14)) // check seated cable
	tb.Connect(ispine.NICs[1].Ports[28], ileaf40g[1].NICs[1].PhysPort(15))
	tb.Connect(ispine.NICs[1].Ports[29], ileaf40g[1].NICs[1].PhysPort(16))
	*/
	tb.Trunk(
		ispine.NICs[1].Ports[26:30],
		ileaf40g[1].NICs[1].PhysPorts(13, 17),
		ileaf40g[1].Id,
		ispine.Id,
	)

	// ileaf1g <--> ispine
	// bond both sides of the breakout
	tb.BreakoutTrunk(
		ispine.NICs[1],
		ispine.NICs[1].PhysPort(31),
		ileaf1g.NICs[2].Ports[0:4],
		ileaf1g.Id,
		ispine.Id,
	)

	// xleaf-xspine ...........................................................

	// xleaf0
	/* XXX
	tb.Connect(xspine[0].NICs[1].Ports[1], xleaf[0].NICs[1].PhysPort(11))
	tb.Connect(xspine[0].NICs[1].Ports[3], xleaf[0].NICs[1].PhysPort(12))
	tb.Connect(xspine[0].NICs[1].Ports[5], xleaf[0].NICs[1].PhysPort(13))
	tb.Connect(xspine[0].NICs[1].Ports[7], xleaf[0].NICs[1].PhysPort(14))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[0].NICs[1].Ports[1],
			xspine[0].NICs[1].Ports[3],
			xspine[0].NICs[1].Ports[5],
			xspine[0].NICs[1].Ports[7],
		},
		xleaf[0].NICs[1].PhysPorts(11, 15),
		xleaf[0].Id,
		xspine[0].Id,
	)
	/* XXX
	tb.Connect(xspine[1].NICs[1].Ports[17], xleaf[0].NICs[1].PhysPort(19))
	tb.Connect(xspine[1].NICs[1].Ports[19], xleaf[0].NICs[1].PhysPort(20))
	tb.Connect(xspine[1].NICs[1].Ports[21], xleaf[0].NICs[1].PhysPort(21))
	tb.Connect(xspine[1].NICs[1].Ports[23], xleaf[0].NICs[1].PhysPort(22))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[1].NICs[1].Ports[17],
			xspine[1].NICs[1].Ports[19],
			xspine[1].NICs[1].Ports[21],
			xspine[1].NICs[1].Ports[23],
		},
		xleaf[0].NICs[1].PhysPorts(19, 23),
		xleaf[0].Id,
		xspine[1].Id,
	)

	// xleaf1
	/* XXX
	tb.Connect(xspine[0].NICs[1].Ports[9], xleaf[1].NICs[1].PhysPort(11))
	tb.Connect(xspine[0].NICs[1].Ports[11], xleaf[1].NICs[1].PhysPort(12))
	tb.Connect(xspine[0].NICs[1].Ports[13], xleaf[1].NICs[1].PhysPort(13))
	tb.Connect(xspine[0].NICs[1].Ports[15], xleaf[1].NICs[1].PhysPort(14))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[0].NICs[1].Ports[9],
			xspine[0].NICs[1].Ports[11],
			xspine[0].NICs[1].Ports[13],
			xspine[0].NICs[1].Ports[15],
		},
		xleaf[1].NICs[1].PhysPorts(11, 15),
		xleaf[1].Id,
		xspine[0].Id,
	)
	/* XXX
	tb.Connect(xspine[1].NICs[1].Ports[25], xleaf[1].NICs[1].PhysPort(19))
	tb.Connect(xspine[1].NICs[1].Ports[27], xleaf[1].NICs[1].PhysPort(20))
	tb.Connect(xspine[1].NICs[1].Ports[29], xleaf[1].NICs[1].PhysPort(21))
	tb.Connect(xspine[1].NICs[1].Ports[31], xleaf[1].NICs[1].PhysPort(22))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[1].NICs[1].Ports[25],
			xspine[1].NICs[1].Ports[27],
			xspine[1].NICs[1].Ports[29],
			xspine[1].NICs[1].Ports[31],
		},
		xleaf[1].NICs[1].PhysPorts(19, 23),
		xleaf[1].Id,
		xspine[1].Id,
	)

	// xleaf2
	/* XXX
	tb.Connect(xspine[0].NICs[1].Ports[17], xleaf[2].NICs[1].PhysPort(19))
	tb.Connect(xspine[0].NICs[1].Ports[19], xleaf[2].NICs[1].PhysPort(20))
	tb.Connect(xspine[0].NICs[1].Ports[21], xleaf[2].NICs[1].PhysPort(21))
	tb.Connect(xspine[0].NICs[1].Ports[23], xleaf[2].NICs[1].PhysPort(22))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[0].NICs[1].Ports[17],
			xspine[0].NICs[1].Ports[19],
			xspine[0].NICs[1].Ports[21],
			xspine[0].NICs[1].Ports[23],
		},
		xleaf[2].NICs[1].PhysPorts(19, 23),
		xleaf[2].Id,
		xspine[0].Id,
	)
	/* XXX
	tb.Connect(xspine[1].NICs[1].Ports[1], xleaf[2].NICs[1].PhysPort(11))
	tb.Connect(xspine[1].NICs[1].Ports[3], xleaf[2].NICs[1].PhysPort(11))
	tb.Connect(xspine[1].NICs[1].Ports[5], xleaf[2].NICs[1].PhysPort(13))
	tb.Connect(xspine[1].NICs[1].Ports[7], xleaf[2].NICs[1].PhysPort(14))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[1].NICs[1].Ports[1],
			xspine[1].NICs[1].Ports[3],
			xspine[1].NICs[1].Ports[5],
			xspine[1].NICs[1].Ports[7],
		},
		xleaf[2].NICs[1].PhysPorts(11, 15),
		xleaf[2].Id,
		xspine[1].Id,
	)

	// xleaf3
	/* XXX
	tb.Connect(xspine[0].NICs[1].Ports[25], xleaf[3].NICs[1].PhysPort(19))
	tb.Connect(xspine[0].NICs[1].Ports[27], xleaf[3].NICs[1].PhysPort(20))
	tb.Connect(xspine[0].NICs[1].Ports[29], xleaf[3].NICs[1].PhysPort(21))
	tb.Connect(xspine[0].NICs[1].Ports[31], xleaf[3].NICs[1].PhysPort(22))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[0].NICs[1].Ports[25],
			xspine[0].NICs[1].Ports[27],
			xspine[0].NICs[1].Ports[29],
			xspine[0].NICs[1].Ports[31],
		},
		xleaf[3].NICs[1].PhysPorts(19, 23),
		xleaf[3].Id,
		xspine[0].Id,
	)
	/* XXX
	tb.Connect(xspine[1].NICs[1].Ports[9], xleaf[3].NICs[1].PhysPort(11))
	tb.Connect(xspine[1].NICs[1].Ports[11], xleaf[3].NICs[1].PhysPort(12))
	tb.Connect(xspine[1].NICs[1].Ports[13], xleaf[3].NICs[1].PhysPort(13))
	tb.Connect(xspine[1].NICs[1].Ports[15], xleaf[3].NICs[1].PhysPort(14))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[1].NICs[1].Ports[9],
			xspine[1].NICs[1].Ports[11],
			xspine[1].NICs[1].Ports[13],
			xspine[1].NICs[1].Ports[15],
		},
		xleaf[3].NICs[1].PhysPorts(11, 15),
		xleaf[3].Id,
		xspine[1].Id,
	)

	// xleaf64 (aka xfabric)
	/* XXX
	tb.Connect(xspine[0].NICs[1].Ports[0], xleaf64.NICs[1].PhysPort(1))
	tb.Connect(xspine[0].NICs[1].Ports[2], xleaf64.NICs[1].PhysPort(2))
	tb.Connect(xspine[0].NICs[1].Ports[4], xleaf64.NICs[1].PhysPort(4))
	tb.Connect(xspine[0].NICs[1].Ports[6], xleaf64.NICs[1].PhysPort(6))
	tb.Connect(xspine[0].NICs[1].Ports[8], xleaf64.NICs[1].PhysPort(9))
	tb.Connect(xspine[0].NICs[1].Ports[10], xleaf64.NICs[1].PhysPort(10))
	tb.Connect(xspine[0].NICs[1].Ports[12], xleaf64.NICs[1].PhysPort(13))
	tb.Connect(xspine[0].NICs[1].Ports[14], xleaf64.NICs[1].PhysPort(14))
	tb.Connect(xspine[0].NICs[1].Ports[16], xleaf64.NICs[1].PhysPort(17))
	tb.Connect(xspine[0].NICs[1].Ports[18], xleaf64.NICs[1].PhysPort(18))
	tb.Connect(xspine[0].NICs[1].Ports[20], xleaf64.NICs[1].PhysPort(21))
	tb.Connect(xspine[0].NICs[1].Ports[22], xleaf64.NICs[1].PhysPort(22))
	tb.Connect(xspine[0].NICs[1].Ports[24], xleaf64.NICs[1].PhysPort(25))
	tb.Connect(xspine[0].NICs[1].Ports[26], xleaf64.NICs[1].PhysPort(26))
	tb.Connect(xspine[0].NICs[1].Ports[28], xleaf64.NICs[1].PhysPort(29))
	tb.Connect(xspine[0].NICs[1].Ports[30], xleaf64.NICs[1].PhysPort(30))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[0].NICs[1].Ports[0],
			xspine[0].NICs[1].Ports[2],
			xspine[0].NICs[1].Ports[4],
			xspine[0].NICs[1].Ports[6],
			xspine[0].NICs[1].Ports[8],
			xspine[0].NICs[1].Ports[10],
			xspine[0].NICs[1].Ports[12],
			xspine[0].NICs[1].Ports[14],
			xspine[0].NICs[1].Ports[16],
			xspine[0].NICs[1].Ports[18],
			xspine[0].NICs[1].Ports[20],
			xspine[0].NICs[1].Ports[22],
			xspine[0].NICs[1].Ports[24],
			xspine[0].NICs[1].Ports[26],
			xspine[0].NICs[1].Ports[28],
			xspine[0].NICs[1].Ports[30],
		},
		[]*xir.Port{
			xleaf64.NICs[1].PhysPort(1),
			xleaf64.NICs[1].PhysPort(2),
			xleaf64.NICs[1].PhysPort(5),
			xleaf64.NICs[1].PhysPort(6),
			xleaf64.NICs[1].PhysPort(9),
			xleaf64.NICs[1].PhysPort(10),
			xleaf64.NICs[1].PhysPort(13),
			xleaf64.NICs[1].PhysPort(14),
			xleaf64.NICs[1].PhysPort(17),
			xleaf64.NICs[1].PhysPort(18),
			xleaf64.NICs[1].PhysPort(21),
			xleaf64.NICs[1].PhysPort(22),
			xleaf64.NICs[1].PhysPort(25),
			xleaf64.NICs[1].PhysPort(26),
			xleaf64.NICs[1].PhysPort(29),
			xleaf64.NICs[1].PhysPort(30),
		},
		xleaf64.Id,
		xspine[0].Id,
	)

	/* XXX
	tb.Connect(xspine[1].NICs[1].Ports[0], xleaf64.NICs[1].PhysPort(33))
	tb.Connect(xspine[1].NICs[1].Ports[2], xleaf64.NICs[1].PhysPort(34))
	tb.Connect(xspine[1].NICs[1].Ports[4], xleaf64.NICs[1].PhysPort(37))
	tb.Connect(xspine[1].NICs[1].Ports[6], xleaf64.NICs[1].PhysPort(38))
	tb.Connect(xspine[1].NICs[1].Ports[8], xleaf64.NICs[1].PhysPort(41))
	tb.Connect(xspine[1].NICs[1].Ports[10], xleaf64.NICs[1].PhysPort(42))
	tb.Connect(xspine[1].NICs[1].Ports[12], xleaf64.NICs[1].PhysPort(45))
	tb.Connect(xspine[1].NICs[1].Ports[14], xleaf64.NICs[1].PhysPort(46))
	tb.Connect(xspine[1].NICs[1].Ports[16], xleaf64.NICs[1].PhysPort(49))
	tb.Connect(xspine[1].NICs[1].Ports[18], xleaf64.NICs[1].PhysPort(50))
	tb.Connect(xspine[1].NICs[1].Ports[20], xleaf64.NICs[1].PhysPort(53))
	tb.Connect(xspine[1].NICs[1].Ports[22], xleaf64.NICs[1].PhysPort(54))
	tb.Connect(xspine[1].NICs[1].Ports[24], xleaf64.NICs[1].PhysPort(57))
	tb.Connect(xspine[1].NICs[1].Ports[26], xleaf64.NICs[1].PhysPort(58))
	tb.Connect(xspine[1].NICs[1].Ports[28], xleaf64.NICs[1].PhysPort(61))
	tb.Connect(xspine[1].NICs[1].Ports[30], xleaf64.NICs[1].PhysPort(62))
	*/
	tb.Trunk(
		[]*xir.Port{
			xspine[1].NICs[1].Ports[0],
			xspine[1].NICs[1].Ports[2],
			xspine[1].NICs[1].Ports[4],
			xspine[1].NICs[1].Ports[6],
			xspine[1].NICs[1].Ports[8],
			xspine[1].NICs[1].Ports[10],
			xspine[1].NICs[1].Ports[12],
			xspine[1].NICs[1].Ports[14],
			xspine[1].NICs[1].Ports[16],
			xspine[1].NICs[1].Ports[18],
			xspine[1].NICs[1].Ports[20],
			xspine[1].NICs[1].Ports[22],
			xspine[1].NICs[1].Ports[24],
			xspine[1].NICs[1].Ports[26],
			xspine[1].NICs[1].Ports[28],
			xspine[1].NICs[1].Ports[30],
		},
		[]*xir.Port{
			xleaf64.NICs[1].PhysPort(33),
			xleaf64.NICs[1].PhysPort(34),
			xleaf64.NICs[1].PhysPort(37),
			xleaf64.NICs[1].PhysPort(38),
			xleaf64.NICs[1].PhysPort(41),
			xleaf64.NICs[1].PhysPort(42),
			xleaf64.NICs[1].PhysPort(45),
			xleaf64.NICs[1].PhysPort(46),
			xleaf64.NICs[1].PhysPort(49),
			xleaf64.NICs[1].PhysPort(50),
			xleaf64.NICs[1].PhysPort(53),
			xleaf64.NICs[1].PhysPort(54),
			xleaf64.NICs[1].PhysPort(57),
			xleaf64.NICs[1].PhysPort(58),
			xleaf64.NICs[1].PhysPort(61),
			xleaf64.NICs[1].PhysPort(62),
		},
		xleaf64.Id,
		xspine[1].Id,
	)

	// Infrastructure server connections ======================================
	tb.Connect(mleaf[3].NICs[1].Ports[46], infraservers[0].NICs[1].Ports[0])
	tb.Connect(ispine.NICs[1].Ports[0], infraservers[0].NICs[2].Ports[1])

	// don't mark the Gw() connectivity, realization will try to create a bgp peer
	// over it which we don't want
	//tb.Connect(ispine.NICs[1].Ports[1], infraservers[0].NICs[2].Ports[0])

	// trunk split
	/*
		tb.Trunk(
			ispine.NICs[1].Ports[0:2],
			infraservers[0].NICs[3].Ports[0:2],
			infraservers[0].Id,
			ispine.Id,
		)
	*/

	assignMacs()

}

func assignMacs() {

	// Mgmt ===================================================================

	// BMC ============
	buoys[0].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:79"
	buoys[1].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:63"
	buoys[2].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:1f"
	buoys[3].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:4a"
	buoys[4].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:30"
	buoys[5].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:3f"
	buoys[6].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:d9"
	buoys[7].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:0e"
	buoys[8].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:12"
	buoys[9].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:ed"
	buoys[10].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:e6"
	buoys[11].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:d3"
	buoys[12].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:60"
	buoys[13].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:69"
	buoys[14].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:40"
	buoys[15].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:d0"
	buoys[16].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:d1"
	buoys[17].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:dc"
	buoys[18].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:48"
	buoys[19].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:e1"
	buoys[20].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:6c"
	buoys[21].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:6f"
	buoys[22].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:1a"
	buoys[23].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:10"
	buoys[24].NICs[0].Ports[0].Mac = "0c:c4:7a:af:c4:83"
	buoys[25].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:d6"
	buoys[26].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:66"
	buoys[27].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:25"
	buoys[28].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:78"
	buoys[29].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:e7"
	buoys[30].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:70"
	buoys[31].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:0f"
	buoys[32].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:ee"
	buoys[33].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:44"
	buoys[34].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:eb"
	buoys[35].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:38"
	buoys[36].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:6e"
	buoys[37].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:ef"
	buoys[38].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:21"
	buoys[39].NICs[0].Ports[0].Mac = "0c:c4:7a:af:c4:84"
	buoys[40].NICs[0].Ports[0].Mac = "0c:c4:7a:af:c4:86"
	buoys[41].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:df"
	buoys[42].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:04"
	buoys[43].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:5f"
	buoys[44].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:f0"
	buoys[45].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:7c"
	buoys[46].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:6a"
	buoys[47].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:cb"
	buoys[48].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:f4"
	buoys[49].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:6d"
	buoys[50].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:52"
	buoys[51].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:c9"
	buoys[52].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:61"
	buoys[53].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:e2"
	buoys[54].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:20"
	buoys[55].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:24"
	buoys[56].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:34"
	buoys[57].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:e4"
	buoys[58].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:11"
	buoys[59].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:65"
	//buoys[60].NICs[0].Ports[0].Mac = "missing"
	buoys[61].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:fd"
	buoys[62].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:1b"
	buoys[63].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:d2"
	buoys[64].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:fe"
	buoys[65].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:d5"
	buoys[66].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:0c"
	buoys[67].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:32"
	//buoys[68].NICs[0].Ports[0].Mac = "missing"
	buoys[69].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:31"
	buoys[70].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:47"
	buoys[71].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:ce"
	buoys[72].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:2d"
	buoys[73].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:71"
	buoys[74].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:0d"

	beacons[0].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:49"
	beacons[1].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:dd"
	beacons[2].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:ca"
	beacons[3].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:da"
	beacons[4].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:6b"
	beacons[5].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:05"
	beacons[6].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:62"
	beacons[7].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:45"
	beacons[8].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:46"
	beacons[9].NICs[0].Ports[0].Mac = "0c:c4:7a:dd:0e:ae"

	emus[0].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:f2"
	emus[1].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:0a"
	emus[2].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:0b"
	emus[3].NICs[0].Ports[0].Mac = "0c:c4:7a:af:d9:cf"
	emus[4].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:22"
	emus[5].NICs[0].Ports[0].Mac = "0c:c4:7a:af:da:08"

	// Mgmt eth0 =======
	buoys[0].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:4e"
	buoys[1].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:9e"
	buoys[2].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:92"
	buoys[3].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:f0"
	buoys[4].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:bc"
	buoys[5].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:da"
	buoys[6].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:0a"
	buoys[7].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:78"
	buoys[8].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:80"
	buoys[9].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:38"
	buoys[10].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:28"
	buoys[11].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:02"
	buoys[12].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:98"
	buoys[13].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:70"
	buoys[14].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:dc"
	buoys[15].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:fc"
	buoys[16].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:fe"
	buoys[17].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:10"
	buoys[18].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:ec"
	buoys[19].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:20"
	buoys[20].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:6c"
	buoys[21].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:62"
	buoys[22].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:90"
	buoys[23].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:7c"
	buoys[24].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:bb:72"
	buoys[25].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:08"
	buoys[26].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:a4"
	buoys[27].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:60"
	buoys[28].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:54"
	buoys[29].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:2c"
	buoys[30].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:64"
	buoys[31].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:7a"
	buoys[32].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:34"
	buoys[33].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:e4"
	buoys[34].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:32"
	buoys[35].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:cc"
	buoys[36].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:68"
	buoys[37].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:3a"
	buoys[38].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:8e"
	buoys[39].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:bb:74"
	buoys[40].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:ae"
	buoys[41].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:1c"
	buoys[42].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:14"
	buoys[43].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:96"
	buoys[44].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:36"
	buoys[45].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:4c"
	buoys[46].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:6e"
	buoys[47].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:f2"
	buoys[48].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:42"
	buoys[49].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:66"
	buoys[50].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:7c"
	buoys[51].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:ee"
	buoys[52].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:9a"
	buoys[53].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:1a"
	buoys[54].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:90"
	buoys[55].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:62"
	buoys[56].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:c4"
	buoys[57].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:1e"
	buoys[58].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:7e"
	buoys[59].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:a2"
	buoys[60].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:04"
	buoys[61].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:58"
	buoys[62].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:92"
	buoys[63].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:00"
	buoys[64].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:96"
	buoys[65].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:5e"
	buoys[66].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:74"
	buoys[67].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:c0"
	buoys[68].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:c2"
	buoys[69].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:be"
	buoys[70].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:ea"
	buoys[71].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:f8"
	buoys[72].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:b6"
	buoys[73].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:5e"
	buoys[74].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:76"

	beacons[0].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:ee"
	beacons[1].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:18"
	beacons[2].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:f0"
	beacons[3].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:0c"
	beacons[4].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:6a"
	beacons[5].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:66"
	beacons[6].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:91:9c"
	beacons[7].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:e6"
	beacons[8].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:e8"
	beacons[9].NICs[1].Ports[0].Mac = "0c:c4:7a:d9:51:d0"

	emus[0].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:3c"
	emus[1].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:70"
	emus[2].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:72"
	emus[3].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:fa"
	emus[4].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:92:98"
	emus[5].NICs[1].Ports[0].Mac = "0c:c4:7a:d8:93:6c"

	// Infranet ===============================================================

	// collected from il0
	buoys[0].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:48"
	buoys[1].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:f0"
	buoys[2].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:e4"
	buoys[3].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:fa"
	buoys[4].NICs[2].Ports[0].Mac = "0c:42:a1:69:22:68"
	buoys[5].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:e8"
	buoys[6].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:76"
	buoys[7].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:c0"
	buoys[8].NICs[2].Ports[0].Mac = "0c:42:a1:c4:76:0c"
	buoys[9].NICs[2].Ports[0].Mac = "0c:42:a1:c4:76:08"
	buoys[10].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:ec"
	buoys[11].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:a0"
	buoys[12].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:d8"
	buoys[13].NICs[2].Ports[0].Mac = "0c:42:a1:6e:0d:36"
	buoys[14].NICs[2].Ports[0].Mac = "0c:42:a1:5f:94:e8"
	buoys[15].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:0c"
	buoys[16].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:24"
	buoys[17].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:a6"
	buoys[18].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:20"
	buoys[19].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:64"
	buoys[20].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:a2"
	buoys[21].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:66"
	buoys[22].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:50"
	buoys[23].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:be"
	buoys[24].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:5c"
	buoys[25].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:9c"
	buoys[26].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:9a"
	buoys[27].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:d4"
	buoys[28].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:52"
	buoys[29].NICs[2].Ports[0].Mac = "0c:42:a1:5f:94:ac"
	buoys[30].NICs[2].Ports[0].Mac = "0c:42:a1:4f:02:c2"
	buoys[31].NICs[2].Ports[0].Mac = "0c:42:a1:0d:d5:e4"
	buoys[32].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:d0"
	buoys[33].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:d6"
	buoys[34].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:cc"
	buoys[35].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:54"
	buoys[36].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:08"
	buoys[37].NICs[2].Ports[0].Mac = "0c:42:a1:69:21:90"
	buoys[38].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:72"
	buoys[39].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:70"

	// collected from il1
	buoys[40].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:c8"
	buoys[41].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:58"
	buoys[42].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:10"
	buoys[43].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:7e"
	buoys[44].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:92"
	buoys[45].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:f8"
	buoys[46].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:c0"
	buoys[47].NICs[2].Ports[0].Mac = "0c:42:a1:69:21:94"
	buoys[48].NICs[2].Ports[0].Mac = "0c:42:a1:c4:76:00"
	buoys[49].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:c4"
	buoys[50].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:50"
	buoys[51].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:62"
	buoys[52].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:dc"
	buoys[53].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:d0"
	buoys[54].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:6c"
	buoys[55].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:ba"
	buoys[56].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:5c"
	buoys[57].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:b8"
	buoys[58].NICs[2].Ports[0].Mac = "0c:42:a1:c4:76:10"
	buoys[59].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:bc"
	//buoys[60].NICs[2].Ports[0].Mac = ""
	buoys[61].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:1c"
	buoys[62].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:cc"
	buoys[63].NICs[2].Ports[0].Mac = "0c:42:a1:79:8a:80"
	buoys[64].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:e0"
	buoys[65].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:f2"
	buoys[66].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:9e"
	buoys[67].NICs[2].Ports[0].Mac = "0c:42:a1:4f:02:a2"
	//buoys[68].NICs[2].Ports[0].Mac = ""
	buoys[69].NICs[2].Ports[0].Mac = "0c:42:a1:c4:75:c4"
	buoys[70].NICs[2].Ports[0].Mac = "0c:42:a1:c4:76:1c"
	buoys[71].NICs[2].Ports[0].Mac = "0c:42:a1:79:8b:54"
	buoys[72].NICs[2].Ports[0].Mac = "0c:42:a1:d2:96:6a"
	buoys[73].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:f6"
	buoys[74].NICs[2].Ports[0].Mac = "0c:42:a1:4e:f1:ce"

	// collected from il3

	beacons[0].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:ef"
	beacons[1].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:19"
	beacons[2].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:92:f1"
	beacons[3].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:0d"
	beacons[4].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:91:6b"
	beacons[5].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:67"
	beacons[6].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:91:9d"
	beacons[7].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:e7"
	beacons[8].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:e9"
	beacons[9].NICs[1].Ports[1].Mac = "0c:c4:7a:d9:51:d1"

	emus[0].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:3d"
	emus[1].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:71"
	emus[2].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:73"
	emus[3].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:92:fb"
	emus[4].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:92:99"
	emus[5].NICs[1].Ports[1].Mac = "0c:c4:7a:d8:93:6d"

	// XpNet ==================================================================

	// collected from xpl0

	buoys[0].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:46:63"
	buoys[0].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:46:62"
	buoys[0].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:46:61"
	buoys[0].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:46:60"

	buoys[1].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:63"
	buoys[1].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:62"
	buoys[1].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:61"
	buoys[1].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:60"

	buoys[2].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:aa:eb"
	buoys[2].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:aa:ea"
	buoys[2].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:aa:e9"
	buoys[2].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:aa:e8"

	buoys[3].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:8b"
	buoys[3].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:8a"
	buoys[3].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:89"
	buoys[3].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:88"

	buoys[4].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ee:fb"
	buoys[4].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ee:fa"
	buoys[4].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ee:f9"
	buoys[4].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ee:f8"

	buoys[5].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ed:db"
	buoys[5].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ed:da"
	buoys[5].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ed:d9"
	buoys[5].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ed:d8"

	buoys[6].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:3f:a3"
	buoys[6].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:3f:a2"
	buoys[6].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:3f:a1"
	buoys[6].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:3f:a0"

	buoys[7].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ef:ab"
	buoys[7].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ef:aa"
	buoys[7].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ef:a9"
	buoys[7].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ef:a8"

	buoys[8].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:eb"
	buoys[8].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:ea"
	buoys[8].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:e9"
	buoys[8].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:e8"

	buoys[9].NICs[3].Ports[3].Mac = "3c:fd:fe:b0:fa:73"
	buoys[9].NICs[3].Ports[2].Mac = "3c:fd:fe:b0:fa:72"
	buoys[9].NICs[3].Ports[1].Mac = "3c:fd:fe:b0:fa:71"
	buoys[9].NICs[3].Ports[0].Mac = "3c:fd:fe:b0:fa:70"

	buoys[10].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ec:b3"
	buoys[10].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ec:b2"
	buoys[10].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ec:b1"
	buoys[10].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ec:b0"

	buoys[11].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:43:43"
	buoys[11].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:43:42"
	buoys[11].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:43:41"
	buoys[11].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:43:40"

	buoys[12].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:f0:03"
	buoys[12].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:f0:02"
	buoys[12].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:f0:01"
	buoys[12].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:f0:00"

	buoys[13].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ee:eb"
	buoys[13].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ee:ea"
	buoys[13].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ee:e9"
	buoys[13].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ee:e8"

	buoys[14].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ec:63"
	buoys[14].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ec:62"
	buoys[14].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ec:61"
	buoys[14].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ec:60"

	buoys[15].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:47:a3"
	buoys[15].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:47:a2"
	buoys[15].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:47:a1"
	buoys[15].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:47:a0"

	buoys[16].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:46:53"
	buoys[16].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:46:52"
	buoys[16].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:46:51"
	buoys[16].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:46:50"

	buoys[17].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ee:83"
	buoys[17].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ee:82"
	buoys[17].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ee:81"
	buoys[17].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ee:80"

	buoys[18].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:e3"
	buoys[18].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:e2"
	buoys[18].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:e1"
	buoys[18].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:e0"

	buoys[19].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ed:3b"
	buoys[19].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ed:3a"
	buoys[19].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ed:39"
	buoys[19].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ed:38"

	buoys[20].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ef:bb"
	buoys[20].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ef:ba"
	buoys[20].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ef:b9"
	buoys[20].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ef:b8"

	// collected from xpl1

	buoys[21].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ec:cb"
	buoys[21].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ec:ca"
	buoys[21].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ec:c9"
	buoys[21].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ec:c8"

	buoys[22].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ec:bb"
	buoys[22].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ec:ba"
	buoys[22].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ec:b9"
	buoys[22].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ec:b8"

	buoys[23].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a9:13"
	buoys[23].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a9:12"
	buoys[23].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a9:11"
	buoys[23].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a9:10"

	buoys[24].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ec:ab"
	buoys[24].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ec:aa"
	buoys[24].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ec:a9"
	buoys[24].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ec:a8"

	buoys[25].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:43:b3"
	buoys[25].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:43:b2"
	buoys[25].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:43:b1"
	buoys[25].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:43:b0"

	buoys[26].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:aa:6b"
	buoys[26].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:aa:6a"
	buoys[26].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:aa:69"
	buoys[26].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:aa:68"

	buoys[27].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:93"
	buoys[27].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:92"
	buoys[27].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:91"
	buoys[27].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:90"

	buoys[28].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ed:4b"
	buoys[28].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ed:4a"
	buoys[28].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ed:49"
	buoys[28].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ed:48"

	buoys[29].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a8:73"
	buoys[29].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a8:72"
	buoys[29].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a8:71"
	buoys[29].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a8:70"

	buoys[30].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:ab:3b"
	buoys[30].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:ab:3a"
	buoys[30].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:ab:39"
	buoys[30].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:ab:38"

	buoys[31].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ed:a3"
	buoys[31].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ed:a2"
	buoys[31].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ed:a1"
	buoys[31].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ed:a0"

	buoys[32].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a9:cb"
	buoys[32].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a9:ca"
	buoys[32].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a9:c9"
	buoys[32].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a9:c8"

	buoys[33].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a8:93"
	buoys[33].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a8:92"
	buoys[33].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a8:91"
	buoys[33].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a8:90"

	buoys[34].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:f0:53"
	buoys[34].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:f0:52"
	buoys[34].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:f0:51"
	buoys[34].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:f0:50"

	buoys[35].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:f0:b3"
	buoys[35].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:f0:b2"
	buoys[35].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:f0:b1"
	buoys[35].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:f0:b0"

	buoys[36].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ee:9b"
	buoys[36].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ee:9a"
	buoys[36].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ee:99"
	buoys[36].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ee:98"

	buoys[37].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:43:bb"
	buoys[37].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:43:ba"
	buoys[37].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:43:b9"
	buoys[37].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:43:b8"

	buoys[38].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a7:93"
	buoys[38].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a7:92"
	buoys[38].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a7:91"
	buoys[38].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a7:90"

	buoys[39].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a7:23"
	buoys[39].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a7:22"
	buoys[39].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a7:21"
	buoys[39].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a7:20"

	// collected from xpl2

	buoys[40].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a7:eb"
	buoys[40].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a7:ea"
	buoys[40].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a7:e9"
	buoys[40].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a7:e8"

	buoys[41].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:aa:0b"
	buoys[41].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:aa:0a"
	buoys[41].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:aa:09"
	buoys[41].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:aa:08"

	buoys[42].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:45:a3"
	buoys[42].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:45:a2"
	buoys[42].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:45:a1"
	buoys[42].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:45:a0"

	buoys[43].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:f0:0b"
	buoys[43].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:f0:0a"
	buoys[43].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:f0:09"
	buoys[43].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:f0:08"

	buoys[44].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a9:1b"
	buoys[44].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a9:1a"
	buoys[44].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a9:19"
	buoys[44].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a9:18"

	buoys[45].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:45:73"
	buoys[45].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:45:72"
	buoys[45].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:45:71"
	buoys[45].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:45:70"

	buoys[46].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ee:43"
	buoys[46].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ee:42"
	buoys[46].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ee:41"
	buoys[46].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ee:40"

	buoys[47].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:aa:e3"
	buoys[47].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:aa:e2"
	buoys[47].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:aa:e1"
	buoys[47].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:aa:e0"

	buoys[48].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:4b"
	buoys[48].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:4a"
	buoys[48].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:49"
	buoys[48].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:48"

	buoys[49].NICs[3].Ports[3].Mac = "3c:fd:fe:b0:fe:bb"
	buoys[49].NICs[3].Ports[2].Mac = "3c:fd:fe:b0:fe:ba"
	buoys[49].NICs[3].Ports[1].Mac = "3c:fd:fe:b0:fe:b9"
	buoys[49].NICs[3].Ports[0].Mac = "3c:fd:fe:b0:fe:b8"

	buoys[50].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ec:73"
	buoys[50].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ec:72"
	buoys[50].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ec:71"
	buoys[50].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ec:70"

	buoys[51].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:2a:1b"
	buoys[51].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:2a:1a"
	buoys[51].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:2a:19"
	buoys[51].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:2a:18"

	buoys[52].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:43:63"
	buoys[52].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:43:62"
	buoys[52].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:43:61"
	buoys[52].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:43:60"

	buoys[53].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:83"
	buoys[53].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:82"
	buoys[53].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:81"
	buoys[53].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:80"

	buoys[54].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:db"
	buoys[54].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:da"
	buoys[54].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:d9"
	buoys[54].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:d8"

	buoys[55].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ef:9b"
	buoys[55].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ef:9a"
	buoys[55].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ef:99"
	buoys[55].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ef:98"

	buoys[56].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:46:83"
	buoys[56].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:46:82"
	buoys[56].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:46:81"
	buoys[56].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:46:80"

	buoys[57].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:2b"
	buoys[57].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:2a"
	buoys[57].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:29"
	buoys[57].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:28"

	buoys[58].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ed:5b"
	buoys[58].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ed:5a"
	buoys[58].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ed:59"
	buoys[58].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ed:58"

	buoys[59].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:43:2b"
	buoys[59].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:43:2a"
	buoys[59].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:43:29"
	buoys[59].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:43:28"

	// collected from xpl3

	buoys[61].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:f1:f3"
	buoys[61].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:f1:f2"
	buoys[61].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:f1:f1"
	buoys[61].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:f1:f0"

	buoys[62].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a9:83"
	buoys[62].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a9:82"
	buoys[62].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a9:81"
	buoys[62].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a9:80"

	buoys[63].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a9:c3"
	buoys[63].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a9:c2"
	buoys[63].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a9:c1"
	buoys[63].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a9:c0"

	buoys[64].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:a8:db"
	buoys[64].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:a8:da"
	buoys[64].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:a8:d9"
	buoys[64].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:a8:d8"

	buoys[65].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:46:9b"
	buoys[65].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:46:9a"
	buoys[65].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:46:99"
	buoys[65].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:46:98"

	buoys[66].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ef:63"
	buoys[66].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ef:62"
	buoys[66].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ef:61"
	buoys[66].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ef:60"

	buoys[67].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ef:5b"
	buoys[67].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ef:5a"
	buoys[67].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ef:59"
	buoys[67].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ef:58"

	buoys[69].NICs[3].Ports[3].Mac = "3c:fd:fe:9d:44:8b"
	buoys[69].NICs[3].Ports[2].Mac = "3c:fd:fe:9d:44:8a"
	buoys[69].NICs[3].Ports[1].Mac = "3c:fd:fe:9d:44:89"
	buoys[69].NICs[3].Ports[0].Mac = "3c:fd:fe:9d:44:88"

	buoys[70].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ed:eb"
	buoys[70].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ed:ea"
	buoys[70].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ed:e9"
	buoys[70].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ed:e8"

	buoys[71].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:5b"
	buoys[71].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:5a"
	buoys[71].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:59"
	buoys[71].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:58"

	buoys[72].NICs[3].Ports[3].Mac = "3c:fd:fe:9f:ab:13"
	buoys[72].NICs[3].Ports[2].Mac = "3c:fd:fe:9f:ab:12"
	buoys[72].NICs[3].Ports[1].Mac = "3c:fd:fe:9f:ab:11"
	buoys[72].NICs[3].Ports[0].Mac = "3c:fd:fe:9f:ab:10"

	buoys[73].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:ef:23"
	buoys[73].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:ef:22"
	buoys[73].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:ef:21"
	buoys[73].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:ef:20"

	buoys[74].NICs[3].Ports[3].Mac = "3c:fd:fe:9e:eb:33"
	buoys[74].NICs[3].Ports[2].Mac = "3c:fd:fe:9e:eb:32"
	buoys[74].NICs[3].Ports[1].Mac = "3c:fd:fe:9e:eb:31"
	buoys[74].NICs[3].Ports[0].Mac = "3c:fd:fe:9e:eb:30"

	beacons[0].NICs[2].Ports[1].Mac = "04:3f:72:cf:f0:ee"
	beacons[0].NICs[2].Ports[0].Mac = "04:3f:72:ac:05:ae"

	beacons[1].NICs[2].Ports[1].Mac = "04:3f:72:ac:07:06"
	beacons[1].NICs[2].Ports[0].Mac = "04:3f:72:ac:09:62"

	beacons[2].NICs[2].Ports[1].Mac = "04:3f:72:cf:f0:b2"
	beacons[2].NICs[2].Ports[0].Mac = "04:3f:72:ac:07:92"

	beacons[3].NICs[2].Ports[1].Mac = "04:3f:72:ac:08:06"
	beacons[3].NICs[2].Ports[0].Mac = "04:3f:72:ac:09:66"

	beacons[4].NICs[2].Ports[1].Mac = "04:3f:72:cf:f0:ce"
	beacons[4].NICs[2].Ports[0].Mac = "04:3f:72:ac:05:8a"

	beacons[5].NICs[2].Ports[1].Mac = "04:3f:72:ac:07:ee"
	beacons[5].NICs[2].Ports[0].Mac = "04:3f:72:ac:08:2e"

	beacons[6].NICs[2].Ports[1].Mac = "04:3f:72:cf:f0:e6"
	beacons[6].NICs[2].Ports[0].Mac = "04:3f:72:cf:f0:f2"

	beacons[8].NICs[2].Ports[1].Mac = "04:3f:72:cf:f0:da"
	beacons[8].NICs[2].Ports[0].Mac = "04:3f:72:cf:f0:f6"

	beacons[9].NICs[2].Ports[1].Mac = "04:3f:72:ac:07:02"
	beacons[9].NICs[2].Ports[0].Mac = "04:3f:72:ac:05:aa"

	emus[0].NICs[2].Ports[1].Mac = "04:3f:72:ac:06:5a"
	emus[0].NICs[2].Ports[0].Mac = "04:3f:72:cf:f0:d6"

	emus[1].NICs[2].Ports[1].Mac = "04:3f:72:ac:06:fa"
	emus[1].NICs[2].Ports[0].Mac = "04:3f:72:ac:07:16"

	emus[2].NICs[2].Ports[1].Mac = "04:3f:72:ac:09:6a"
	emus[2].NICs[2].Ports[0].Mac = "04:3f:72:ac:06:8e"

	emus[3].NICs[2].Ports[1].Mac = "04:3f:72:ac:06:ea"
	emus[3].NICs[2].Ports[0].Mac = "04:3f:72:ac:09:5e"

	emus[4].NICs[2].Ports[1].Mac = "04:3f:72:ac:06:6e"
	emus[4].NICs[2].Ports[0].Mac = "04:3f:72:ac:06:86"

	emus[5].NICs[2].Ports[1].Mac = "04:3f:72:cf:f0:9a"
	emus[5].NICs[2].Ports[0].Mac = "04:3f:72:ac:05:8e"

	infraservers[0].NICs[0].Ports[0].Mac = "18:c0:4d:4f:6c:8d"
	infraservers[0].NICs[1].Ports[0].Mac = "18:c0:4d:4f:6c:8b"
	infraservers[0].NICs[2].Ports[0].Mac = "04:3f:72:b7:19:64"
	infraservers[0].NICs[2].Ports[1].Mac = "04:3f:72:b7:19:65"

	infraservers[1].NICs[0].Ports[0].Mac = "18:c0:4d:4f:6c:81"
	infraservers[1].NICs[1].Ports[0].Mac = "18:c0:4d:4f:6c:7f"
	infraservers[1].NICs[2].Ports[0].Mac = "04:3f:72:b7:14:fc"
	infraservers[1].NICs[2].Ports[1].Mac = "04:3f:72:b7:14:fd"

	ileaf1g.NICs[0].Ports[0].Mac = "14:02:ec:2d:b0:00"
	ileaf40g[0].NICs[0].Ports[0].Mac = "48:0f:cf:ae:88:18"
	ileaf40g[1].NICs[0].Ports[0].Mac = "48:0f:cf:ae:2b:c0"
	ispine.NICs[0].Ports[0].Mac = "48:0f:cf:ae:d9:6c"
	xleaf[0].NICs[0].Ports[0].Mac = "04:3f:72:ab:fe:24"
	xleaf[1].NICs[0].Ports[0].Mac = "0c:42:a1:bb:31:3a"
	xleaf[2].NICs[0].Ports[0].Mac = "04:3f:72:ea:0e:2e"
	xleaf[3].NICs[0].Ports[0].Mac = "04:3f:72:ab:fd:a4"
	xleaf64.NICs[0].Ports[0].Mac = "04:3f:72:c4:1d:a0"
	xspine[0].NICs[0].Ports[0].Mac = "04:3f:72:ab:fe:44"
	xspine[1].NICs[0].Ports[0].Mac = "04:3f:72:ab:fd:e4"

}

func iPorts(rs []*xir.Resource) []*xir.Port {

	var ps []*xir.Port
	for _, r := range rs {
		ps = append(ps, r.Infranet())
	}
	return ps

}

/*
func idealCabling(tb *Builder) {

	i := 0
	for _, x := range buoys {
		tb.Connect(
			x.NextEth(), mleaf[i/48].NextSwp(),
			RJ45(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)
		i++
	}

	for j := 0; j < len(buoys); j += 4 {
		tb.Breakout(
			ileaf40g[j/(len(buoys)/2)].NICs[1],
			ileaf40g[j/(len(buoys)/2)].NextSwp(),
			[]*xir.Port{
				buoys[j].Infranet(),
				buoys[j+1].Infranet(),
				buoys[j+2].Infranet(),
				buoys[j+3].Infranet(),
			},
			DAC(),
			QSFPP_4xSFPP(),
			Product("Mellanox", "QSFPP 4x QSFPP 5m Breakout DAC", "MC2609125-005"),
		)
	}

	x := 0
	for _, b := range buoys {
		tb.Breakout(
			xleaf[x/20].NICs[1],
			xleaf[x/20].NextSwp(),
			b.NextEthsG(4, 10),
			Product("Approved Optics/10Gtek", "QSFPP 4x QSFPP Breakout DAC", "CAB-QSFP/4SFP-P3M-A"),
		)
		x++
	}

	for _, x := range beacons {
		tb.Connect(
			x.NextEth(),
			mleaf[i/48].NextSwp(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)

		tb.Connect(
			x.NextEth(),
			ileaf1g.NextSwp(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)

		tb.Connect(
			x.NextEthG(100),
			xleaf64.NextSwp(),
			Product("Mellanox", "100G QSFP28 DAC 3M", "MCP1600-E003E26"),
		)

		tb.Connect(
			x.NextEthG(100),
			xleaf64.NextSwp(),
			Product("Mellanox", "100G QSFP28 DAC 3M", "MCP1600-E003E26"),
		)

		i++
	}

	for _, x := range infraservers {
		tb.Connect(
			x.NextEth(),
			mleaf[i/48].NextSwp(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)
		i++
	}
	for _, x := range stor {
		tb.Connect(
			x.NextEth(),
			mleaf[i/48].NextSwp(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)
		i++
	}

	tb.Connect(
		ileaf1g.NextEth(),
		mleaf[i/48].NextSwp(),
		Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
	)
	i++

	for _, x := range ileaf40g {
		tb.Connect(
			x.NextEth(),
			mleaf[i/48].NextSwp(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)
		i++
	}

	tb.Connect(
		ispine.NextEth(),
		mleaf[i/48].NextSwp(),
		Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
	)
	i++

	tb.Connect(
		xleaf64.NextEth(),
		mleaf[i/48].NextSwp(),
		Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
	)
	i++

	for _, x := range xleaf {
		tb.Connect(
			x.NextEth(),
			mleaf[i/48].NextSwp(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)
		i++
	}

	for _, x := range xspine {
		tb.Connect(
			x.NextEth(),
			mleaf[i/48].NextSwp(),
			Product("Generic", "Cat6 RJ45 Cable", "GENERIC_CAT6"),
		)
		i++
	}

	for _, x := range mleaf {
		tb.Connect(
			x.NextSwpG(10),
			mspine.NextSwpG(10),
			Product("Mellanox", "SFP+ DAC 7m", "CAB-QSFP/4SFP-P3M-A"),
		)

		tb.Connect(
			x.NextSwpG(10),
			mspine.NextSwpG(10),
			Product("Mellanox", "SFP+ DAC 7m", "CAB-QSFP/4SFP-P3M-A"),
		)

		tb.Connect(
			x.NextSwpG(10),
			mspine.NextSwpG(10),
			Product("Mellanox", "SFP+ DAC 7m", "CAB-QSFP/4SFP-P3M-A"),
		)

		tb.Connect(
			x.NextSwpG(10),
			mspine.NextSwpG(10),
			Product("Mellanox", "SFP+ DAC 7m", "CAB-QSFP/4SFP-P3M-A"),
		)
	}

	tb.Breakout(
		ispine.NICs[1],
		ispine.NextSwp(),
		ileaf1g.NextSwpsG(4, 10),
		Product("FS", "QSFPP to 4xSFPP Optical Breakout", "SFP-10GSR-85/QSFP-SR4-40G/8FMTPLCOM4"),
	)

	for j := 0; j < 8; j++ {
		for _, x := range ileaf40g {
			tb.Connect(
				x.NextSwp(),
				ispine.NextSwp(),
				Product("Approved Optics/10Gtek", "QSFPP 40G 5M DAC", "CAB-QSFP/QSFP-P5M"),
			)
		}
	}

	for _, l := range xleaf {
		for _, s := range xspine {
			tb.Connect(
				l.NextSwp(),
				s.NextSwp(),
				Product("Mellanox", "100G QSFP28 DAC 5M", "MCP1600-E005E26"),
			)

			tb.Connect(
				l.NextSwp(),
				s.NextSwp(),
				Product("Mellanox", "100G QSFP28 DAC 5M", "MCP1600-E005E26"),
			)

			tb.Connect(
				l.NextSwp(),
				s.NextSwp(),
				Product("Mellanox", "100G QSFP28 DAC 5M", "MCP1600-E005E26"),
			)

			tb.Connect(
				l.NextSwp(),
				s.NextSwp(),
				Product("Mellanox", "100G QSFP28 DAC 5M", "MCP1600-E005E26"),
			)
		}
	}

	for _, s := range xspine {

		for j := 0; j < 16; j++ {
			tb.Connect(
				xleaf64.NextSwp(),
				s.NextSwp(),
				Product("Approved Optics/10Gtek", "QSFPP 40G 5M DAC", "CAB-QSFP/QSFP-P5M"),
			)
		}

	}

	for _, x := range infraservers {
		tb.Connect(
			x.NextEthG(100),
			ispine.NextSwp(),
			Product("Mellanox", "100G QSFP28 DAC 3M", "MCP1600-E003E26"),
		)
	}

	for _, x := range stor {
		tb.Connect(
			x.NextEthG(100),
			ispine.NextSwp(),
			Product("Mellanox", "100G QSFP28 DAC 3M", "MCP1600-E003E26"),
		)
	}

}
*/
